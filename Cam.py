import cv2
img_name = input("enter a name of the person that in the picture(without spaces):")
img_name+=".jpg"
cam = cv2.VideoCapture(0)

cv2.namedWindow("Cam")

while True:
    ret, frame = cam.read()
    cv2.imshow("Cam", frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27:
        # ESC pressed
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed
        cv2.imwrite(img_name, frame)

cam.release()

cv2.destroyAllWindows()