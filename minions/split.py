import os
import shutil
import random

images_path = 'frames/'
labels_path = 'xmls/'
train_path = 'dataset/train/'
validation_path = 'dataset/validation/'
os.makedirs(train_path +'/images')
os.makedirs(train_path +'/annotations')
os.makedirs(validation_path +'/images')
os.makedirs(validation_path +'/annotations')


for image_file in os.listdir(images_path):
    print(image_file)    
    labels_file = image_file.replace('.jpg', '.xml')
    if random.uniform(0, 1) > 0.2:
        shutil.copy(images_path + image_file, train_path + 'images/' + image_file)
        shutil.copy(labels_path + labels_file, train_path + 'annotations/' + labels_file)
    else:
        shutil.copy(images_path + image_file, validation_path +
                    'images/' + image_file)
        shutil.copy(labels_path + labels_file, validation_path +
                    'annotations/' + labels_file)