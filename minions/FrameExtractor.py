import cv2
import shutil
import os
import glob

vidcap = cv2.VideoCapture('minions.mp4')
success,image = vidcap.read()
count = 0
while success:
  if (count % 4 == 0):
    count = count/4
    cv2.imwrite("frame%d.jpg" % count, image)      
    count = count*4
  success,image = vidcap.read()
  print('Read a new frame: ', success)
  count += 1

