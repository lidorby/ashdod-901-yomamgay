import unittest
import os
import sys
from faceReq import FaceRec
import face_recognition
import cv2
import numpy as np
import glob

class TestFaceReq(unittest.TestCase):
    name = "lidor"
    def test_FaceRecAcc(self):
        returned_val = FaceRec()
        is_acc = 0
        print(returned_val)
        for i in returned_val:
            if self.name in i or self.name == i:
                is_acc += 1
        self.assertAlmostEqual(is_acc,len(returned_val))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        TestFaceReq.name = sys.argv.pop()
    print(TestFaceReq.name)
    unittest.main()