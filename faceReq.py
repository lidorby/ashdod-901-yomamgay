import face_recognition
import cv2
import glob




def get_Faces_data(frame):
    known_face_encodings = []
    known_face_names = []
    for filename in glob.glob('images/*.jpg'): #assuming gif
        curr_images = face_recognition.load_image_file(filename)
        curr_encoding = face_recognition.face_encodings(curr_images)[0]
        known_face_encodings.append(curr_encoding)
        known_face_names.append(filename.split(".")[0])
    temp_list = []
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_frame = frame[:, :, ::-1]

    # Find all the faces and face enqcodings in the frame of video
    face_locations = face_recognition.face_locations(rgb_frame)
    face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)

    # Loop through each face in this frame of video
    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

        name = "Unknown"

        # If a match was found in known_face_encodings, just use the first one.
        if True in matches:
            first_match_index = matches.index(True)
            name = known_face_names[first_match_index]
        print(name)
        temp_list.append((name,((left,top),(right,bottom))))
    return temp_list

def FaceRec():
    known_face_encodings = []
    known_face_names = []
    for filename in glob.glob('images/*.jpg'): #assuming gif
        curr_images = face_recognition.load_image_file(filename)
        curr_encoding = face_recognition.face_encodings(curr_images)[0]
        known_face_encodings.append(curr_encoding)
        known_face_names.append(filename.split(".")[0])
    list_of_faces = []
    # Get a reference to webcam #0 (the default one)
    video_capture = cv2.VideoCapture(0)
    while True:
        temp_list = []
        ret, frame = video_capture.read()

        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_frame = frame[:, :, ::-1]

        # Find all the faces and face enqcodings in the frame of video
        face_locations = face_recognition.face_locations(rgb_frame)
        face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
        # Loop through each face in this frame of video
        for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)

            name = "Unknown"

            # If a match was found in known_face_encodings, just use the first one.
            if True in matches:
                first_match_index = matches.index(True)
                name = known_face_names[first_match_index]
            
            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
            list_of_faces.append(name)
        cv2.imshow('Video', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()
    return list_of_faces

if __name__ == "__main__":
    FaceRec()
